﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace TokoWaikiki_App.Win10
{
    class Koneksi
    {
        private MySqlConnection koneksi = null;
        private static Koneksi dbkoneksi = null;

        private Koneksi()
        {
            if(koneksi == null)
            {
                string server = "localhost";
                string database = "mahasiswa";
                string user = "root";
                string password = "";
                string strkoneksi = "SERVER=" + server + ";DATABASE=" + database + ";UID=" + user + ";PASSWORD=" + password;
                koneksi = new MySqlConnection(strkoneksi);
                koneksi.Open();

            }
        }

        public static Koneksi GetInstance()
        {
            if(dbkoneksi == null)
            {
                dbkoneksi = new Koneksi();
            }

            return dbkoneksi;
        }

        public MySqlConnection GetConnection()
        {
            return this.koneksi;
        }
    }
}
